#include <iostream>
#include <string>

using namespace std;

string translate(string line, string argument){
	string result = ""; // implementeer dit
	int x = stoi(argument);
	x = x % 26;
	char letter;
	for(unsigned int i = 0; i <  line.size(); i++){
		if((line[i] <= 90 && line[i] + x > 90 ) || (line[i] >= 97 && line[i] + x > 122)){
			letter = line[i] - 26 + x;
		}
		else if(line[i] == ' '){
			letter = ' ';
		}
		else {
			letter = line[i] + x;
		}
		result.push_back(letter);
	}
  	return result; 
}

int main(int argc, char *argv[])
{ string line;

  if(argc != 2)
  {cerr << "Deze functie heeft exact 1 argument nodig" << endl;
    return -1; }

  while(getline(cin, line))
  {cout << translate(line, argv[1]) << endl;} 
	
  return 0; }
