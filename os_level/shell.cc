#include "shell.hh"

int main()
{ std::string input;

  // ToDo: Vervang onderstaande regel: Laad prompt uit bestand 


  int fd = syscall(SYS_open, "prompt.txt", O_RDONLY, 0755);
  char byte[1];                                           
  std::string prompt = "";  
  while(syscall(SYS_read, fd, byte, 1)){          
    prompt.append(byte);
  }
  

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file() // ToDo: Implementeer volgens specificatie.
{
  std::string bestandsnaam, eerste_regel;

  std::cout << "Voer een bestandsnaam in: ";
  std::cin >> bestandsnaam;
  bestandsnaam.append(".txt");

  std::cout << "Voer eerste regel in: ";
  std::getline(std::cin, eerste_regel);
  
  std::cout << eerste_regel;
  int bestand = syscall(SYS_creat, bestandsnaam.c_str(), 0775);
  syscall(SYS_write , bestand, eerste_regel, 100);
  syscall(SYS_close, bestandsnaam);

}

void list() // ToDo: Implementeer volgens specificatie.
{ 



}


void find() // ToDo: Implementeer volgens specificatie.
{ std::cout << "ToDo: Implementeer hier find()" << std::endl; }

void seek() // ToDo: Implementeer volgens specificatie.
{ 
  std::string naam = "loop.txt"; 
  
  int bestand_seek = syscall(SYS_creat, "seek.txt", 0775);
  int bestand_loop = syscall(SYS_creat, "loop.txt", 0775);
  //printen met loop
  syscall(SYS_write, bestand_loop, "X", 1);
  for(unsigned int i = 0; i < 5*1024*1024; i++){
    syscall(SYS_write, bestand_loop, "\0", 1);

  }
  syscall(SYS_write, bestand_loop, "X", 1);
  syscall(SYS_close, bestand_loop);

  //printen met seek
  syscall(SYS_write, bestand_seek, "X", 1);
  syscall(SYS_lseek, bestand_seek, 5*1024*1024, SEEK_CUR);
  syscall(SYS_write, bestand_seek, "X", 1);
  syscall(SYS_close, bestand_seek);

  

}

void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.
